Tutorial material for the kmos3 KMC simulation package.
This material was originally developed by Juan M. Lorenzi and Mie Andersen (Technical University of Munich) for hands-on-sessions using the kmos code held during the
<div align="center">
College on Multiscale Computational Modeling of Materials for Energy Applications
</div>
in the ICTP, Trieste, Italy, 7-8 July 2016.

The material was updated in 2022 by Mie Andersen (Aarhus University) and Meelod Waheed (Oak Ridge National Laboratory) and 2024 by Martin Deimel (Fritz Haber Institute).
