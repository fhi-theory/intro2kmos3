\documentclass[intro2kmos3.tex]{subfiles}

\begin{document}

\onlyinsubfile{\pagestyle{main}}
\notinsubfile{}

\chapter{Building a \texttt{kmos3} Model}

    Now that we know how to run an existing \texttt{kmos3} model, we will learn how to generate a new model. While doing this we will learn about the way models are abstractly represented. We will also discuss how the models are saved and how the source code necessary for running the simulations is generated and compiled.

    \section{The Elements of a \texttt{kmos3} Project}

        A \texttt{kmos3} model is built by putting together several building blocks as illustrated in \Cref{fig:kmos3_model}. Those building blocks are found within \texttt{kmos3.types}.

        \begin{figure}
            \centering
            \includegraphics[width=0.67\textwidth]{images/kmos3_model.png}
            \caption{Structure of a kmos3 model.}
            \label{fig:kmos3_model}
        \end{figure}

        \subsection{Project}

            The \texttt{Project} is the structure that contains all other elements.

        \subsection{Meta}

            \texttt{Meta} contains the meta data of the project. This includes the model's name, the author's name and email, and, importantly, the dimensionality (1D, 2D or 3D) of the system.

        \subsection{Layer}
        
            A \texttt{kmos3} \texttt{Project} contains a list of \texttt{Layer} objects. In this tutorial we will only consider models with a single layer, but more could be used. Each layer has a unique name, an optional representation, and a list of sites objects \texttt{Site}. Each of those sites has a unique name and a position in the unit cell.

        \subsection{Species}

            A project also contains a list of \texttt{Species}. Each species needs to have a unique name. A color and a representation can optionally be added; these are useful for using the viewer \gls{GUI}.

        \subsection{Parameter}

            A list of parameters is included in a \texttt{Project}. Each parameter needs a unique name and a default value. Parameters are used in the definition of the rate constants, and their values can be modified at run-time using the \gls{API} or (if defined as adjustable) the viewer \gls{GUI}.

        \subsection{Process}

            The processes are perhaps the most complex structure in a \texttt{kmos3} model, and they are built using the elements previously discussed. Apart from a unique name, processes are composed of a list of conditions, a list of actions and a rate constant expression. Optionally, a process can also contain a \texttt{tof\_count} attribute if it contributes to some of the \glspl{TOF}.

            \paragraph{Condition} A condition is defined by selecting a single species and a single coordinate. A coordinate is a site, but containing additionally information about its offset, i.e., the relative position of this coordinate with respect to a reference central coordinate. The list of conditions of a process encodes the (local) occupation pattern necessary for the process to occur.

            \paragraph{Action} Like conditions, actions are also composed of a species and a coordinate. The list of actions of a process determines what is changed in the system state if the process is actually executed.
            
            \paragraph{Rate Constant} Each process needs to be assigned a rate constant expression. These are given as strings that can contain common mathematical expression, any of the user-defined parameters as well as some other predefined parameters and constants. All rate constants are evaluated when the model is first loaded and whenever the value of a parameter is changed.

        \subsection{Thermochemistry} \label{subsec:thermochemistry}

            The enthalpy and entropy of a species (and thus of a reaction, and thus of activation energies) change as a function of temperature. These temperature dependent effects on the activation energies can be captured using thermochemistry terms inside of the activation energies of the rate expressions. \texttt{kmos3} currently has two types of thermochemistry methods, for which we both provide examples in this repository. The first one is based on the \gls{NIST} \gls{JANAF} tables and the second one the \texttt{thermochemistry} module of the \gls{ASE}. We will refer to the two approaches as "\gls{JANAF} way" and "\gls{ASE} way" for simplicity.

            The \gls{JANAF} approach is roughly as follows: There are empirical equations (in particular, Shomate equations) for describing the enthalpy and entropy temperature dependencies of gas molecules using multi-term polynomial expressions -- these take into account deviations from an ideal gas. These are typically from experimental measurements and are very accurate. They can be extended to surface species. The \gls{JANAF} tables data files are essentially tables of coefficients that are equivalent to the Shomate equation (there is also a similar approach for \gls{NASA} polynomials, which is not supported by \texttt{kmos3}, though it is possible to convert \gls{NASA} polynomials to the \gls{JANAF} format). This approach we already used with \menu{COoxRuO2\_\_build.py}.
            
            The \gls{ASE} way is roughly as follows: There are statistical mechanics equations which can be used to describe the most strongly contributing terms to the temperature dependence, which are entropy terms. These entropy terms are related to vibrations, rotations, and translations. The \gls{ASE} way currently uses some crude approximations for the entropy terms, with parameters that are calculated from electronic structure calculations. Despite its crudeness, the \gls{ASE} way is sufficiently accurate for most studies%
            % (as will be further explained in \Cref{sec:ASE_JANAF_comparison})
            . An example can be found in the file \menu{COoxRuO2\_\_build\_w\_ase.py}.

    \section{\texorpdfstring{A Model Step-By-Step: O\textsubscript{2} Adsorption / Desorption}{A Model Step-By-Step: O2 Adsorption / Desorption}}

        We will learn about the components of \texttt{kmos3} models through a simple example: A model for oxygen adsorption and desorption onto a \gls{fcc}(100) (square) lattice (c.f. \Cref{fig:O2_model}).

        \begin{figure}
            \centering
            \includegraphics[width=0.6\textwidth]{images/o2_model.png}
            \caption{\Gls{O2} adsorption / desorption model processes.}
            \label{fig:O2_model}
        \end{figure}

        The only two possible processes are the dissociative adsorption with the rate constant $k_{\text{ads}}$ and the associative desorption with the rate constant $k_{\text{des}}$. Implement this model by following these steps (you can also take a look at the \menu{COoxRuO2\_\_build.py} file):

        \subsubsection{TASK \thetask: O\textsubscript{2} Adsorption / Desorption Model}
        \stepcounter{task}

        \begin{enumerate}
            \item Start by importing \texttt{kmos3}
            \item Initialize a new model with the \texttt{create\_kmc\_model()}
            \item Set projects metadata: author, email,  model name and model dimension
            \item Define a layer with one site, add it to the project
            \item Add surface species
            \item Define model parameters ($k_{\text{ads}}$ \& $k_{\text{des}}$) with a value of 1
            \item Generate auxiliary coordinates as the steps involve more than one surface site
            \item Define the processes (avoid double counting)
            \item Save and compile the model
        \end{enumerate}


    % \section{TASK \thetask: The ZGB Model}
    % \stepcounter{task}

    %     The \gls{ZGB} model \cite{Ziff1986} is a classic example of a \gls{KMC} model of \gls{CO} oxidation. It includes dissociative \gls{O2} adsorption, molecular \gls{CO} adsorption and a \gls{CO}--\gls{O} reaction to form gaseous \gls{CO2}. The only parameter in the model is the fraction of \gls{CO} in the gas mixture y\gls{CO}. The \gls{CO} adsorption rate per unit cell on empty sites is equal to y\gls{CO}. The \gls{O2} adsorption rate per unit cell is $(1 - y\gls{CO})$. Desorption of the reactants (\gls{CO} and \gls{O2}) is neglected. To avoid deadlocks, i.e., states in which no process can happen, desorption processes with very small rate (i.e., 10$^{-10}$s$^{-1}$) should be included. \gls{CO2} formation has an "infinite" rate constant in the model, which can be modeled by using a very large value (i.e., 10$^{10}$s$^{-1}$).

    %     Hint: Beware of double counting when implementing the \gls{O2} adsorption process.

    % \section{Modeling (Lattice) Diffusion}

    %     \subsection{A Simple Ion Diffusion Model}

    %         Here, we will consider a simple example of particles diffusing on a 2D square lattice. The example render script \menu{render\_LGD.py} can be found in the \menu{task\_material} folder.

    %         A similar simple model in a realistic context (\gls{Li} diffusion in graphene) can be found in Reference \numcite{Lehnert1992}.

    %     \subsection{Preparing the System}

    %         If used as given, trying to run the model will not work. This is because at start-up the system will be completely empty, a state in which no process can occur. To solve this problem there are two possibilities, depending on our objectives:
    %         \begin{itemize}
    %             \item One option is to prepare the system so that it contains a certain number of ions at the start, as was done in \Cref{subsec:initial_state}. This is useful if one ones to study the relaxation of the system from the selected configuration or to study equilibrium properties.
    %             \item Alternatively, it is possible to impose special boundary conditions to the system. In this way it is also possible to study steady-state behavior under non-equilibrium conditions.
    %         \end{itemize}
    %         In this course, we will focus only on the second case.

    %     \subsection{TASK \thetask: Implement the Boundary Conditions}
    %     \stepcounter{task}

    %         The objective of this task is to set up the boundary conditions for the diffusion model. This can be achieved by including a pair of auxiliary species (\texttt{source} and \texttt{drain}) and the corresponding entry and exit processes (as shown in the slides). Additionally, the \menu{kmc\_settings.py} file needs to be modified in two ways:
    %         \begin{itemize}
    %             \item First, the default system size should be changed from the default (20 $\times$ 20) to something that is longer in the direction of prominent diffusion, e.g. (50 $\times$ 20)
    %             \item Second, the \texttt{setup\_model} function should be modified to place the sources and drains in the correct positions.
    %         \end{itemize}
    %         If you manage to set this up correctly, the model can be visualized using \texttt{model.view()} in IPython or \texttt{kmos3 view} from the command line.

        % \subsection{TASK \thetask: Extending and Testing the Lattice Diffusion Model} \label{subsec:lattice_diffusion_model}
        % \stepcounter{task}

        %     Extend the diffusion model to include the effects of an external electric field. For this, introduce a new parameter that measures the strength of the field, and modify the diffusion processes accordingly. Once this is set in place, test how the effects of field strength and particle concentration in the current (details in the slides).

    \section{Lateral Interactions in \texttt{kmos3}}

        Up to now we have only considered models with a relatively small number of processes. In some situations, however, it is possible that the rate constants depend not only on the class of process being executed, but also on the local environment around the adsorbates. For example, the particles in our diffusion problem could interact repulsively, changing the rates of diffusion. In these cases we say that there are lateral interactions in the system. The standard way to treat this in \texttt{kmos3} is to explicitly incorporate all different processes arising from the interactions. In order to do this, we can use the \texttt{itertools} Python module, to programmatically explore possible local states.% An example of this is presented in the slides.

    % \section{TASK \thetask: Solid-On-Solid Crystal Growth Model}
    % \stepcounter{task}

    %     Another problem that can be studied with \gls{KMC} is that of crystal growth. We will consider a very simple model of crystal growth: The Solid-On-Solid (SOS) model. The model we will consider only includes adsorption and desorption processes, neglecting diffusion. The adsorption rate is constant, but the desorption rate is affected by both the system temperature and by lateral interactions (details in the slides).

    %     Interesting examples of use of \gls{KMC} in studies of growth processes can be found in References \numcite{Kratzer2002a, Kratzer2002, Smilauer1993}.

    %     Implement the SOS model in \texttt{kmos3}. Simulate crystal growth for two different temperatures (350~K and 450~K) and observe the resulting structures in both cases.

    %     Please note that in a typical growth process, a "substrate" species layer is required to start the growth. For this purpose in the solution, we use the \texttt{put()} method in the Python file \menu{growth.py} and place some substrate atoms (we use \gls{Pd}) in the first layer. Once the substrate is created, adatoms can be adsorbed on the substrate, and the growth can start. It is also possible to have the starting layer be the same material as the later layers.

    % \section{TASK \thetask: Diffusion in the SOS Model}
    % \stepcounter{task}

    %     Implement diffusion processes in the SOS growth model. Compare growth patterns in the model with and without diffusion. Consider also a model without desorption (only adsorption and diffusion).

    % \section{TASK \thetask: Lateral Interactions in the Diffusion Model}
    % \stepcounter{task}

    %     Recalculate the plots obtained in \Cref{subsec:lattice_diffusion_model} for the diffusion model that includes lateral interaction. Do this for different values of the lateral interaction strength.

    % \section{TASK \thetask: Defects in the Diffusion Model}
    % \stepcounter{task}

    %     Extend the 2D diffusion model by adding a 'defect' species that blocks diffusion. Generate an initialization script that prepares the system with defects randomly located. Test the effect of the presence of defects in the current. Do this for different defect concentrations.

%     \section{TASK \thetask: Comparing Energy Differences for Different Thermochemistry Approximations} \label{sec:ASE_JANAF_comparison}
    % \stepcounter{task}

%         The way that we will compare the \gls{ASE} way vs. the \gls{JANAF} way is to calculate the rate constants for desorption of \gls{CO} with each method as a function of temperature and see how large of a difference in energy these rates would correspond to. We will do the same for \gls{O2}. We will use the files \menu{COoxRuO2\_\_build\_w\_ase.py} and \menu{COoxRuO2\_\_build\_w\_janaf.py}.

%         We will check the temperatures 100~K, 300~K, 500~K, and 800~K (\texttt{TemperaturesToCheck}). The temperatures can be changes with
%         \begin{lstlisting}[language=python]
% model.parameters.T = <T_value>
% \end{lstlisting}
%         Then check the rate constant at each temperature using
%         \begin{lstlisting}[language=python]
% model.rate_constants.by_name('CO_desorption_bridge')
% \end{lstlisting}
        

%         Inside the \menu{solutions} directory, we have the two directories \menu{COoxRuO2\_w\_janaf\_local\_smart} and \menu{COoxRuO2\_w\_ase\_local\_smart}.
%         Inside each of those directories is a file called \menu{Runfile.py} that produces \menu{StackedArray.csv}.
%         The two directories' \menu{StackedArray.csv} values are consolidated in \menu{ThermoChemistryComparison.xlsx} of the solutions. Inside this Excel file, the ratio of the \gls{ASE} rate divided by the \gls{JANAF} rate is taken. The \gls{ASE} rates are lower than the \gls{JANAF} rates, indicating that the \gls{ASE} rates have a higher barrier (so more high energy gas states). For each of the temperatures investigated, we then compare this ratio the expression $e^{(-dE/RT)}$ where $dE$ is difference in energies. Looking at values of 1000 J/mol to 15,000 J/mol in the columns to the right hand side of the Excel file, we see that the \gls{ASE} way differs from the \gls{JANAF} way by around 10,000 J/mol ($\pm$ 5000 J/mol) for this system with the current settings. While this is significant, it is considered within the uncertainties of \gls{DFT} calculations which are around 20,000 J/mol for this application. Thus, we can say that the \gls{ASE} way has been implemented sufficiently correct here, and is adequately accurate for research purposes.

%         There are some caveats about the entropy approximations of the \gls{ASE} way. In addition to the entropy approximations, there is also the \gls{ZPE} correction. The \gls{JANAF} way has \gls{ZPE} correction inherently within it for the gas phase values. The \gls{ASE} way does not. In principle, \texttt{kmos3} may later add the \gls{ZPE} correction but currently does not. The \gls{ZPE} correction is not so important for small molecules ($<$5 atoms) but can become more important for larger molecules. The enthalpy corrections (\gls{ZPE} as well as the population of higher-energy translational, vibrational and rotational energy states at finite temperatures) can actually also be quite large for high-temperature catalysis. See, e.g., Figure \href{https://pubs.acs.org/doi/10.1021/acs.jpcc.9b05642#fig3}{3} in Reference \numcite{Andersen2019a} on graphene synthesis at Cu, which were calculated using more sophisticated \gls{ASE} calculations:  Additionally, for the \gls{ASE} way, \texttt{kmos3} currently assumes all vibrational modes are harmonic and ignores rotational modes. Furthermore, in this example, the vibrational modes of the adsorbate were neglected (which brings the adsorbate energy a little closer to the gas phase energy). It is better to include the adsorbate vibrational modes also.
        
\onlyinsubfile{%
    \glssetwidest{1234567}%
    \printnoidxglossary[type=acronym, title=List of Abbreviations, style=mcolalttreenoline]%
    \printbibliography%
    }

\end{document}