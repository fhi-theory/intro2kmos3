\documentclass[intro2kmos3.tex]{subfiles}

\begin{document}

\onlyinsubfile{\pagestyle{main}}
\notinsubfile{}

\chapter{Introduction}

    \section{What is Kinetic Monte Carlo?}

        \Gls{KMC} is a stochastic simulation technique that allows to achieve a numerical solution to the time evolution of a system that can be described with a Markovian master equation. That is, the system can exist in a given number of discrete states and can switch between these states according to known transition rates that only depend on the current state of the system. Chemical transformations like diffusion in materials, surface catalysis, crystal growth, or deposition on surfaces consist of such transitions of one state into another. Depending on their complexity, they can be very high dimensional and can only be solved using \Gls{KMC}.
        
        In this workshop we will use the open-source \gls{KMC} code \texttt{kmos3}, which is an updated version of the \texttt{kmos} code developed by Max Hofmann.\cite{Hoffmann2014} General introductions to the \gls{KMC} method can be found in References \numcite{Chatterjee2007, Stamatakis2014, Reuter2011, Voter2007} and for a recent tutorial introduction to \gls{KMC} based on the \texttt{kmos} code we refer the reader to Reference \numcite{Andersen2019}.

    \section{\texttt{kmos3}}

        \subsection{Features}
    
            \begin{itemize}
                \item User-friendly Python interface
                \item Viewer \gls{GUI}
                \item Runner \gls{API} (recommended way of running \texttt{kmos3})
                \item Efficient Fortran backends (\texttt{local\_smart}, \texttt{lat\_int}, \texttt{otf})
                \item Lateral adsorbate--adsorbate interactions (\texttt{lat\_int} or \texttt{otf})
                \item Acceleration of quasi-equilibrated processes
                \item Sensitivity analysis
                \item Desired time steps
                \item Snapshots
                % \item Machine learning identification of local configurations (under development)
            \end{itemize}

        \subsection{Installation}
    
            It is recommended to install \texttt{kmos3} on a Debian (derived) operating system within a Python virtual environment. The following instructions as well as the \href{https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3/-/blob/main/INSTALL.rst?ref_type=heads}{installation instructions} of the \href{https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3}{\texttt{kmos3} repository} are written accordingly.
    
            If you plan to use Windows or MacOS, it is recommended to run a Debian (derivative) within \href{https://www.virtualbox.org/}{VirtualBox}.
    
            \subsubsection{VirtualBox (optional)}
    
                \begin{enumerate}
                    \item Download the software from the \href{https://www.virtualbox.org/wiki/Downloads}{official website} and install it on your system
                    \item Download an image (e.g., Debian from the \href{https://www.debian.org/download}{official website})
                    \item Set up VirtualBox (version 7.0)
                    \begin{itemize}
                        \item Open VirtualBox and click on \menu{New} to create a new virtual machine
                        \item Within \menu{Name and Operating System} enter a name for the machine, select the just downloaded image in \menu{ISO Image}, and select \menu{Skip Unattended Installation}
                        \item Allocate at least 8~GB of RAM and 4~CPUs (if possible) within \menu{Hardware}
                        \item Within \menu{Hard Disk} select \menu{Create a Virtual Hard Disk Now} and allocate a minimum of 25~GB
                        \item Click \menu{Finish} to create the virtual machine and press \menu{Start} to start it
                        \item Go through the installation process of the selected operating system
                        \item Open a terminal, add your user account to the \texttt{sudo} group by typing
                        \begin{lstlisting}[language=bash]
su
# enter password
sudo usermod -aG sudo $USER
\end{lstlisting} and restart the system
                    \end{itemize}
                    \item Optional: Change the display resolution of your virtual machine to the desired value
                \end{enumerate}
    
                \begin{note}[Hints]
                    \begin{itemize}
                        \item For full-screen mode with the virtual machine push \keys{\rctrl + F}
                        \item VirtualBox can create snapshots of the current state under \menu{Machine > Tools > Snapshots}. In the snapshots section, you can click \menu{Take} to save a snapshot. In case something goes wrong you have the option to restore the snapshot or clone the snapshot to a new virtual machine
                    \end{itemize}
                \end{note}

            \subsubsection{\texttt{kmos3}}
        
                \begin{enumerate}
                    \item Install the required packages with
                    \begin{lstlisting}[language=bash]
sudo apt-get update
sudo apt-get install -y gfortran python3-dev python3-tk g++ python3-venv git
\end{lstlisting}
                    \item Create a virtual Python environment and activate it
                    \begin{lstlisting}[language=bash]
python3 -m venv $HOME/kmos3_venv
source $HOME/kmos3_venv/bin/activate
\end{lstlisting}
                    \item Install \texttt{kmos3}
                    \begin{lstlisting}[language=bash]
cd $HOME
git clone https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3.git
cd kmos3
pip install .[complete]
\end{lstlisting}
                    \item Test the installation by issuing the following commands
                    \begin{lstlisting}[language=bash]
cd examples
python3 ZGB_model__build.py
cd zgb_model_local_smart
kmos3 benchmark
kmos3 view
\end{lstlisting}  
                \end{enumerate}

                \begin{note}[Notes]
                    \begin{itemize}
                        \item It is important that you always remain in the virtual Python environment
                        \item If you need to exit the virtual environment, enter \texttt{deactivate} in the terminal
                    \end{itemize}
                \end{note}

\onlyinsubfile{%
    \glssetwidest{1234567}%
    \printnoidxglossary[type=acronym, title=List of Abbreviations, style=mcolalttreenoline]%
    \printbibliography%
    }

\end{document}