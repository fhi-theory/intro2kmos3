\documentclass[intro2kmos3.tex]{subfiles}

\begin{document}

\onlyinsubfile{\pagestyle{main}}
\notinsubfile{}

\chapter{Visualization \& Quantitative Analysis}\label{chap:visualization_and_quantitative_analysis}

    \glsunsetall

    First, we need to fetch some material for the following tutorials. Within the terminal, change into your desired directory, e.g., \texttt{\$HOME} for saving this material, and enter the following command:
    \begin{lstlisting}[language=bash]
git clone https://gitlab.mpcdf.mpg.de/fhi-theory/intro2kmos3
\end{lstlisting}

    \section{The Viewer GUI}

        The file \menu{\$HOME>intro2kmos3>task\_material>COoxRuO2\_\_build.py} contains a Python representation of the surface catalysis \gls{KMC} model for the \gls{CO} oxidation reaction ($2\ \gls{CO} + \gls{O2} \rightarrow 2\ \gls{CO2}$) over the \gls{RuO2}(110) surface. You can read more about this model in the original publication~\cite{Reuter2004} and in the tutorial \gls{KMC} review mentioned above.\cite{Andersen2019} In brief, the \gls{RuO2}(110) surface contains two types of adsorption sites, the \gls{br} and \gls{cus} sites, as illustrated in \Cref{fig:RuO2_surface}.

        \begin{figure}
            \centering
            \includegraphics{images/RuO2_surface.jpeg}
            \caption{Top view of the structure of the \gls{RuO2}(110) surface. To the left, the surface unit cell is shown as a black rectangle. Big green spheres represent \glspl{Ru}, small red spheres \glspl{O}. To the right, the coarse-grained lattice structure is sketched. It consists of alternating columns of \gls{cus} and \gls{br} sites.
            Taken from \href{https://doi.org/10.3389/fchem.2019.00202}{Andersen \textit{et al.}, \textit{Front. Chem.} \textbf{7}, 202 (2019)} under the \href{https://creativecommons.org/licenses/by/4.0/}{CC BY 4.0} license.}
            \label{fig:RuO2_surface}
        \end{figure}

        The reactant molecules \gls{CO} and \gls{O2} can adsorb onto both of these site types. \Gls{CO} occupies a single site, whereas \gls{O2} adsorbs dissociatively onto two neighboring sites (i.e., \gls{br}-\gls{br}, \gls{cus}-\gls{cus}, or \gls{br}-\gls{cus}). During the simulation, a given site can thus be empty or covered with \gls{CO} or \gls{O}. Furthermore, \gls{CO} and \gls{O} can diffuse on the surface or react with each other to form \gls{CO2}, which is assumed to desorb immediately into the gas phase (since it binds very weakly to the surface).
    
        Be sure that you are inside the \texttt{kmos3} virtual environment before proceeding. You can do so with this command in the terminal:
        \begin{lstlisting}[language=bash]
source $HOME/kmos3_venv/bin/activate
\end{lstlisting}
        To generate the \gls{CO} oxidation model one needs to change into the \menu{\$HOME>intro2kmos3>task\_material} directory and execute the \menu{COoxRuO2\_\_build.py} file:
        \begin{lstlisting}[language=bash]
cd $HOME/intro2kmos3/task_material
python3 COoxRuO2__build.py
\end{lstlisting}
        This model uses the \gls{JANAF} tables for including temperature dependence, as will be explained in \Cref{subsec:thermochemistry}. In case you would want to modify the model, this is done by modifying the Python script and then saving and compiling the model again.
        
        Two things are created when executing the build file: The \menu{COoxRuO2.xml} file contains the data defining the model in xml format and the directory \menu{COoxRuO2\_local\_smart} contains the compiled model. Note that the files defining the KMC model in \menu{COoxRuO2\_local\_smart} will be overwritten in case they existed, but not any client scripts (see \Cref{sec:client_scripts}). 

        \subsubsection{TASK \thetask: Viewer GUI}
        \stepcounter{task}
        
        The viewer \gls{GUI} is a convenient tool to quickly inspect, study, and manipulate a model's behavior. Use it with the following steps:
        \begin{enumerate}
            \item Change into the directory of the compiled model:
            \begin{lstlisting}[language=bash]
cd COoxRuO2_local_smart
\end{lstlisting}
            \item Start the viewer \gls{GUI}:
            \begin{lstlisting}[language=bash]
kmos3 view
\end{lstlisting}
            \item Set T $\approx$ 600~K and p$_{\gls{O2}}$ $\approx$ 10$^{-1}$~bar. Try to tune the value of p$_{\gls{CO}}$ to get the highest possible value for the \gls{TOF}. For which \gls{CO} partial pressure does this happen? What is the \gls{TOF} maximum? Pay special attention to the state of the catalyst (what's adsorbed on it) when the \gls{TOF} is highest. What happens for other values of p$_{\gls{CO}}$?
            \item Set p$_{\gls{O2}}$ $\approx$ 10$^{-1}$~bar and p$_{\gls{CO}}$ $\approx$ 2 $\times$ 10$^{-1}$~bar and T $\approx$ 450~K. Slowly increase T (in steps of $\approx$ 10~K) up to 600~K, while monitoring the the steady state \gls{TOF}. How does the surface behave?
        \end{enumerate}

    
    \section{Scripting, Plotting and Visualization}

        The \texttt{kmos3} \gls{API} uses the Python programming language. Some basic usage and examples are provided below.

        \subsection{Elements of the Python Syntax}
        
            \begin{enumerate}
                \item (Some of the) Python data types:
                \begin{itemize}
                    \item Basic types:
                    \begin{lstlisting}[language=python]
some_string = 'foo' # strings
some_integer = 134  # integers
some_float = 0.8    # floating point numbers
some_boolean = True # booleans
\end{lstlisting}
                    \item Lists:
                    \begin{lstlisting}[language=python]
L = ['a', 3, 4.7]
L[0] # 'a'
L[1] # 3
L[2] # 4.7
\end{lstlisting}
                    \item Dictionaries:
                    \begin{lstlisting}[language=python]
D = {'Name': 'kmos3', 'Release': 2024, 'pi': 3.14}
D['Release'] # 2024
\end{lstlisting}
                \end{itemize}
                \item Printing \texttt{'Hello World'} on the terminal:
                \begin{lstlisting}[language=python]
print('Hello World')
\end{lstlisting}
                \item Importing modules:
                \begin{lstlisting}[language=python]
import ... [as ...]
from ... import ...
\end{lstlisting}
                \item If statements:
                \begin{lstlisting}[language=python]
if <condition>:
    ...
elif <condition>:
    ...
else:
    ...
\end{lstlisting}
                \item Flow control with for loops:
                \begin{lstlisting}[language=python]
for i in range(3):
    print(i)
\end{lstlisting}
                Output:
                \begin{lstlisting}[language=python]
0
1
2
\end{lstlisting}
                \begin{lstlisting}[language=python]
for i, j in enumerate(['hat', 'cat', 'bat']):
    print(i + ' ' + j)
\end{lstlisting}
                Output:
                \begin{lstlisting}[language=python]
0 hat
1 cat
2 bat
\end{lstlisting}
            \end{enumerate}
            \begin{note}[Documentation]
                \href{https://docs.python.org/3/}{https://docs.python.org/3/}
            \end{note}

        \subsection{Scientific Computing Using the NumPy Package}

            The NumPy package contains many useful things such as multidimensional array objects, linear algebra routines, and random number generators. To use the package you need to import it and usually this is done as follows:
            \begin{lstlisting}[language=python]
import numpy as np
A = np.array([0 , 1])
\end{lstlisting}
            \begin{note}[Documentation]
                \href{https://www.numpy.org/}{https://www.numpy.org/}
            \end{note}

        \subsection{Plotting Using Matplotlib}

            The Python plotting library Matplotlib can be used to create publication quality figures in a variety of formats. Any other plotting software can of course also be used, provided the \texttt{kmos3} output data are stored in an appropriate format. The advantage of using Matplotlib is the easy integration into Python scripts for running \texttt{kmos3}.
            \begin{note}[Documentation]
                \href{https://matplotlib.org/}{https://matplotlib.org/}
            \end{note}

        \subsection{Visualizing Atomic Structures Using the ASE}

            The \gls{ASE} is a set of tools and Python modules developed for atomistic simulations. If installed, it is used in \texttt{kmos3} to visualize the current state of a model.
            \begin{note}[Documentation]
                \href{https://wiki.fysik.dtu.dk/ase/}{https://wiki.fysik.dtu.dk/ase/}
            \end{note}

    \section{First API Steps: Running \texttt{kmos3} Interactively}

        While we can use the \gls{GUI} viewer to get a general idea of the behavior of the model, quantitative analysis requires the use of the \gls{API}. Be sure that you are inside the \texttt{kmos3} virtual environment and that you are in the \menu{COoxRuO2\_local\_smart} directory.

        In order to launch the \gls{API}, change  and run the command:
        \begin{lstlisting}[language=bash]
ipython
\end{lstlisting}
        This will initialize IPython, an interactive Python interface. Inside this interface, we can load our model by typing:
        \begin{lstlisting}[language=python]
from kmos3.run import KMC_Model
model = KMC_Model()
\end{lstlisting}
        These commands will give some text output (including the model's name and author), and create the model object, which we will use to run our simulations.
        
        A useful shortcut is to skip the above and type \texttt{kmos3 run} in the terminal within the folder containing the compiled \gls{KMC} model, which directly opens the interactive shell and creates the model object at once.
        
        The most elementary thing we can do with our model is to directly run it:
        \begin{lstlisting}[language=python]
NSTEPS = 1e6
model.do_steps(NSTEPS)
\end{lstlisting}
        which will run \texttt{NSTEPS} \gls{KMC} steps (in this case 1 million). This will take some time (probably a few seconds), and then we will recover the (Ipython) command prompt.

        Below are listed some other useful \texttt{kmos3} methods, attributes, and commands:
        \begin{enumerate}
            \item Change the value of a parameter (e.g., temperature or pressure):
            \begin{lstlisting}[language=python]
model.parameters.T = 550
model.parameters.p_COgas = 0.5
\end{lstlisting}
            \item To print a list of the current values of all parameters and rate constants in the model, simply type:
            \begin{lstlisting}[language=python]
model
\end{lstlisting}
            \item Print current occupations (as averaged coverages):
            \begin{lstlisting}[language=python]
model.print_coverages()
\end{lstlisting}
            \item Print a list of the number of times each process has been executed since the beginning of the simulation:
            \begin{lstlisting}[language=python]
model.print_procstat()
\end{lstlisting}
            \item Analyze the current state of the model:
            \begin{lstlisting}[language=python]
atoms = model.get_atoms()
\end{lstlisting}
            This command returns an \gls{ASE} atoms object, which can be visualized by typing:
            \begin{lstlisting}[language=python]
model.show()
\end{lstlisting}
            The atoms object also contains some additional data:
            \begin{lstlisting}[language=python]
atoms.occupation
atoms.tof_data
atoms.kmc_time
atoms.kmc_step
\end{lstlisting}
            The occupations and \glspl{TOF} (i.e., the number of produced molecules, here \gls{CO2}, per surface site per second) come in the form of NumPy arrays. In order to get the headers for these arrays, type:
            \begin{lstlisting}[language=python]
model.get_occupation_header()
model.get_tof_header()
\end{lstlisting}
            The \glspl{TOF} that are affiliated with the atoms object are \glspl{TOF} averaged over the simulated time since the last \texttt{get\_atoms()} call. In contrast, \texttt{atoms.occupation} is the current occupation (identical to what is printed by the \texttt{model.print\_coverages()} method).
            \item Sample a model and return average \glspl{TOF} and coverages in a standardized format:
            \begin{lstlisting}[language=python]
model.get_std_sampled_data(samples, sample_size, tof_method='integ', output='str')
\end{lstlisting}
            The parameter \texttt{samples} is the number of batches to average over. The total number of \gls{KMC} steps to sample is given by the \texttt{sample\_size} parameter. The number of steps per batch is consequently \texttt{sample\_size} divided by \texttt{samples}. In each case check carefully that the desired observable is sampled good enough! The parameter \texttt{tof\_method} allows switching between two different methods for evaluating \glspl{TOF}. The default method \texttt{'procstat'} evaluates the number of executed events in the simulated time interval. \texttt{'integ'} will evaluate the number of times the reaction could be evaluated in the simulated time interval based on the local configurations and the rate constant.

            The output of \texttt{model.get\_std\_sampled\_data()} is determined by the parameter output, which can be set to \texttt{'str'} or \texttt{'dict'}. The default \texttt{'str'} returns a text string containing first the values of all adjustable parameters, then the \glspl{TOF} and coverages, and finally the total sampled \gls{KMC} time, the total simulated \gls{KMC} time (including also the time simulated before the \texttt{get\_std\_sampled\_data()} call), and the number of sampled \gls{KMC} steps, all separated by spaces. This text string can be converted to a Python list using the Python \texttt{split()} method:
            \begin{lstlisting}[language=python]
data = model.get_std_sampled_data(samples=10, sample_size=1e6)
data_list = data.split(' ')
print(data_list)
\end{lstlisting}
            Alternatively, \texttt{output='dict'} returns the above information in the form of a Python dictionary. For example, the \gls{TOF} for \gls{CO} oxidation can be retrieved using the command:
            \begin{lstlisting}[language=python]
output_dict = model.get_std_sampled_data(samples=10, sample_size=1e6, output = 'dict')
print(output_dict)
TOF_CO = output_dict['CO_oxidation']
print(TOF_CO)
\end{lstlisting}
            \item Access to the Fortran modules:
            The above commands are often sufficient when running and simulating a \texttt{kmos3} model, but in certain cases direct access to the Fortran data structures and methods is desirable. The Fortran modules \texttt{base}, \texttt{lattice}, and \texttt{proclist} are attributes of the model within \texttt{kmc\_model.*.so}. An instance of this model is imported and can be explored using IPython and <TAB>:
            \begin{lstlisting}[language=python]
model.base.<TAB>
model.lattice.<TAB>
model.proclist.<TAB>
\end{lstlisting}
            \item Deallocating memory:
            \begin{lstlisting}[language=python]
model.deallocate()
\end{lstlisting}
            This command is important to call once a simulation has finished, if you want to run a new simulation within the same script, as it frees the memory allocated by the Fortran modules.

            Reset model:
            \begin{lstlisting}[language=python]
model.reset()
\end{lstlisting}
            This command is called after \texttt{model.deallocate()} in order to reset the model to its initial state.
        \end{enumerate}

    \section{Client Scripts} \label{sec:client_scripts}

        We have already seen that the \texttt{kmos3} viewer \gls{GUI} is useful to quickly investigate model behavior and that interactive use of the \gls{API} allows for finer control and more precise quantitative evaluation. In everyday use, however, the most useful way of using \texttt{kmos3} is through client scripts. With client scripts we can automatize the tasks we performed using the interactive interface.

        % \subsection{TASK \thetask: Snapshots, Time per Snapshot, a Temperature Programmed Reaction Template}
        % \stepcounter{task}

        %     We are going to use \menu{task\_material>MyFirstTPD\_Precovered\_\_build.py}. This example uses the snapshots feature which is one of the possible ways of running simulations in \texttt{kmos3}. This example also introduces users to the \texttt{time\_per\_snapshot} feature of \texttt{kmos3}, which is useful for transient kinetics studies, such as temperature programmed reactions. In a temperature programmed reaction, the temperature is changed with time (usually as a linear function of time, as in this example).

        %     This example shows how to run a temperature programmed reaction with \texttt{kmos3} and the \texttt{tps} (time per snapshot) feature. The directory for the model contains the \menu{runfile7.py} file, which will be used for this workshop exercise. This file can then act as a template for temperature programmed reaction studies.
            
        %     If the user would like to learn more after this workshop, there are additional examples inside \menu{examples>MyFirstTPD\_Precovered\_local\_smart} of the \texttt{kmos3} source code.
            
        %     After compiling the model by running the build file, take a look at \menu{runfile7.py} in the model's directory.

        %     Looking inside the file:
        %     \begin{itemize}
        %         \item There are some loading features, which we ignore for now
        %         \item Further down, we see that we set the \texttt{sps} (steps per snapshot) and the \texttt{tps} between lines 50 and 60. The \texttt{tps} is set to \texttt{1.0}, which means that anytime a snapshot reaches beyond 1 second of simulation time, the snapshot will end without doing further steps (so the actual steps per snapshot will not be constant). For example, if \texttt{sps=1000} but only 43 steps are reached within this 1 second, then the snapshot will end at 43 steps instead of finishing all 1000 steps.
        %         \item In this file for the temperature programmed reaction we also set the initial temperature, final temperature, and heating rate for the temperature programmed reaction. Near the bottom of the file is a loop that loops over the temperatures.
        %     \end{itemize}

        %     Now, run this file. After completion, open the \glspl{TOF} file produced by the snapshots module. Make a scatter plot with the x axis as temperature and the y axis as the \texttt{TOF\_data}; make another for \texttt{TOF\_integ}. Save these plots. Now, open the file, change the \texttt{tps} to 0.1, run it again, and make the same plots.

        \subsubsection{TASK \thetask: Arrhenius Plots}
        \stepcounter{task}

            Here, we will create a client script to illustrate how to make an Arrhenius plot (i.e., log(\gls{TOF}) vs. 1/T) for the \gls{RuO2} \gls{CO} oxidation model. This type of analysis is used heavily in the catalysis community for understanding temperature effects on reaction rates and how this relates to the reaction mechanism. In case the reaction mechanism involves a single thermally activated \gls{RLS}, the Arrhenius plot will be a straight line from which the apparent activation energy, $E_{\mathrm{a}}$, and the pre-exponential factor, $A$, can be determined, i.e.,
            \begin{equation}
                r = A \exp{\left( \frac{-E_{\mathrm{a}}}{k_{\mathrm{B}} T} \right)} \quad \text{,}
            \end{equation}
            where $r$ is the reaction rate (same as \gls{TOF}) and $k_{\mathrm{B}}$ is the Boltzmann constant. A detailed discussion of the meaning of the apparent activation energy can be found in Reference \numcite{Mao2019}.

            % Be sure that you are in the \texttt{kmos3} virtual environment. Run the \menu{COoxRuO2\_\_build\_w\_ase.py} file and change into the \menu{COoxRuO2\_local\_smart} directory, where you also find the example script \menu{plot\_arrhenius.py}. You can run it using the following command (note that this may take around 10 minutes to run):
            In the \menu{COoxRuO2\_local\_smart} directory of the viewer task, you also find the example script \menu{plot\_arrhenius.py}. You can run it using the following command (note that this may take around 10 minutes to run):
            \begin{lstlisting}[language=python]
python3 plot_arrhenius.py
\end{lstlisting}
            After the script is finished observe the generated plot and try to explain your observations.
            
            Note that \menu{plot\_arrhenius.py} script does not save the obtained \glspl{TOF}. In case you would just want to modify the plot using data obtained previously, it can be useful to save the data to a file. An example of how to save and re-load the data is provided in the script \menu{plot\_arrhenius\_w\_file.py}.

        % \subsection{TASK \thetask: TOF and Coverages vs. Pressure Diagrams}
        % \stepcounter{task}

        %     The objective of this task is to write a client script similar to the one from the previous task. In this case, you need to plot both \gls{TOF} and coverages (discriminated by site type and species) as a function of \gls{CO} partial pressure, for constant temperature $T = 600~\mathrm{K}$ and $p_{\gls{O2}} = 0.1~\mathrm{bar}$ (note that the default units for temperature and pressure in \texttt{kmos3} are Kelvin and bar). The plot should cover values within $10^{-1}~\mathrm{bar} \leq p_{\gls{CO}} \leq 10^2~\mathrm{bar}$ (log-scale should be used). Note that the partial pressures of the gas phase species affect the model through the rate constants for adsorption, $k^{\mathrm{ad}}$. These are derived from kinetic gas theory and given by the rate of impingement of the gas phase species $i$ onto the site $s$ of area $A_{st}$,
        %     \begin{equation}
        %         k^{\mathrm{ad}}_{i,st}(T, p_i) = S_{i,st}(T) \frac{p_i A_{st}}{\sqrt{2 \pi m_i k_{\mathrm{B}} T}} \quad \text{,}
        %     \end{equation}
        %     where $p_i$ and $m_i$ are the partial pressure and mass, respectively, of species $i$. $S_{i,st}$ is the temperature dependent sticking coefficient of species $i$ onto the site $st$, which is often assumed to be unity if no other information is available. You can find all of the rate constant expressions used in the \gls{CO} oxidation model in the file \menu{kmc\_settings.py}.

        %     You can use the previous example as a basis and either directly use Matplotlib or another plotting tool by writing the data to a file. The strings that label the output important for this task in the dictionary output from the \texttt{model.get\_std\_sampled\_data()} function are \texttt{'CO\_oxidation'}, \texttt{'CO\_ruo2\_bridge'}, \texttt{'CO\_ruo2\_cus'}, \texttt{'O\_ruo2\_bridge'} and \texttt{'O\_ruo2\_cus'}.

        %     The solution to this task can be found in \menu{solutions>plot\_vs\_pCO.py}.

        \subsection{Relaxing the System}

            We either begin the \gls{KMC} simulation with a clean surface or prepare the system in some user-specified initial state% (see \Cref{subsec:initial_state_effect})
            . In any case, this initial system state might be very different from the steady-state system state. It is therefore necessary to run a number of \gls{KMC} steps to relax the system before any meaningful information about steady-state \glspl{TOF} and coverages can be obtained. It should thus always be checked that the system has reached steady-state before calling \texttt{model.get\_std\_sampled\_data()}! An example showing how this can be done is provided in the script \menu{relaxation.py}.

        \subsection{Preparing the Initial State of the System} \label{subsec:initial_state}

            Since it can be difficult to estimate if the system has reached steady state or not, it can be useful to test different initial states of the system to check that the same steady-state solution is always found, independently of the initial state. The state of the system can be modified at any time during the simulation, but most often one would want to prepare a given initial state before starting the simulation. The occupation of a site \texttt{<site>} can be changed to the species \texttt{<species>} using the command:
            \begin{lstlisting}[language=python]
model.put(site=[x,y,z,model.lattice.<site>], model.proclist.<species>)
\end{lstlisting}
            where \texttt{x}, \texttt{y}, and \texttt{z} are the coordinates of the site. The above command can be quite inefficient if changing many sites at once, since each \texttt{put()} call adjusts the book-keeping database. To circumvent this you can use the \texttt{\_put()} method instead:
            \begin{lstlisting}[language=python]
model._put(...)
model._put(...)
...
model._adjust_database()
\end{lstlisting}
        and adjust the book-keeping database once with \texttt{\_adjust\_database()}.
        Uncomment lines 16-19 in the script \menu{relaxation.py} for an example of how to use this.
        
        In some cases, one might have a good guess of the steady-state system state in terms of (averaged) coverages. This could for example be obtained by solving the corresponding model in the \gls{MFA} using rate equations. In case the reader is not familiar with what is implied by the \gls{MFA} in chemical kinetics, we refer to Reference \numcite{Temel2007} for an introduction.

        % \subsection{TASK \thetask: The effect of the initial state} \label{subsec:initial_state_effect}
        % \stepcounter{task}

        %     Try relaxing the model from different initial states, e.g., clean, CO@br, O@cus, etc., by doing the corresponding modifications to the script \menu{relaxation.py}. Also try to vary the number of \gls{KMC} steps taken in each sample (\texttt{sample\_step}) and the number of samples (\texttt{N}).

        % \subsection{TASK \thetask: Random Initial State from Guess Coverages}
        % \stepcounter{task}

        %     Using a good guess for the final coverages can substantially speed up the time required to relax the system. Since the \gls{MFA} does not consider lattice inhomogeneity, the best way to convert the \gls{MFA} coverages to a \gls{KMC} lattice occupation is to assume a random occupation of the lattice sites. In order to do this, the guess coverages must be converted to the number of sites occupied by each species. For each species, the corresponding number of sites is then chosen randomly among the total number of available sites in the system using the Python \texttt{random.sample()} method. Take a look at the script \menu{relaxation\_random\_initialization.py} and try relaxing the model from different random initial states.

        \subsection{Sensitivity Analysis}

            In a catalytic system, the macroscopic \gls{TOF} is often controlled by only one or a few microscopic rate constants. This can be quantified using the so-called \gls{DRC}.\cite{Stegelmann2009} Several definitions of this concept exist, but here we will be concerned with quantifying how much the change of the rate constant $k$ of a single microscopic process $i$ ($+$ for forward process, $-$ for reverse process) affects the \gls{TOF} according to the formula
            \begin{equation}
                x_i^+ = \frac{k_i^+}{\mathrm{TOF}} \left. \frac{\partial \mathrm{TOF}}{\partial k_i^+} \right\vert_{k_{j \neq i}^+, K_{j \neq i}, k_{j}^-} \quad \text{,}
            \end{equation}
            where $x_i^+$ is the \gls{DRC} of the forward process $i$, $k_{j \neq i}^+$ are the rate constants of all other forward processes $j$ different from $i$, $K_{j \neq i}$ are the equilibrium constants (i.e., the ratio between the forward and the reverse process) of processes $j$ different from $i$, and $k_{j}^-$ are the rate constants of all reverse processes $j$ (including the reverse process $i$).

            % \subsection{TASK \thetask: Sensitivity Analysis}
            % \stepcounter{task}
            
            % Take a look at the script \menu{DRC.py}, which calculates the \gls{DRC} for \gls{CO} adsorption onto the \gls{cus} site. The derivative is calculated using finite differences by changing the rate constant by plus and minus 2\% (the delta value). Try to play around with the delta value and the number of sample steps used to get an averaged \gls{TOF}. Finite differences are extremely sensitive to numerical noise and therefore require very long sampling times to sufficiently converge. Does your result match the result for the appropriate process and \gls{CO} pressure in the lower panel of Figure \href{https://www.sciencedirect.com/science/article/pii/S0039602809000752#fig4}{4} in Reference \numcite{Meskine2009}? Note that there may be smaller differences since the used \gls{CO} oxidation model is not completely identical to the literature one.

%         \subsection{TASK \thetask: ModelRunner}
        % \stepcounter{task}

%             For some \gls{KMC} applications you simply require a large number of data points across a set of external parameters (phase diagrams, microkinetic models). For this case there is the convenient class \texttt{ModelRunner} to work with. For example:
%             \begin{lstlisting}[language=python]
% from kmos3.run import ModelRunner, PressureParameter, TemperatureParameter

% class ScanKinetics(ModelRunner):
%     p_02gas= PressureParameter(1)
%     T = TemperatureParameter(600)
%     p_COgas = PressureParameter(min=1e-1, max=1e-2, steps=20)

% if __name__ == "__main__":
%     ScanKinetics().run(init_steps=1e7, sample_steps=1e7, cores=4)
% \end{lstlisting}

%             This script generates data points over the specified \gls{CO} partial pressure range. Using the \texttt{PressureParameter} or \texttt{TemperatureParameter} assures that the corresponding parameters will be sampled in a log- or reciprocal-scale, respectively. The script above runs several \texttt{kmos3} jobs synchronously (as many as indicated with the cores argument) and generates an output file with results (in this case called \texttt{ScanKinetics.dat}).

%             Try to reproduce the Arrhenius plot using the \texttt{ModelRunner} class. Compare the time spent by the original scripts with this one. Use as many cores as your workstation has.

%             Hint: \texttt{ModelRunner} uses a file to keep track of which calculations took place. If you need to restart the calculations from scratch, you have to remove the file ending in \texttt{.lock}. Also delete the old \texttt{ScanKinetics.dat} file, since otherwise the new calculations will be appended to the old file.

%             The solution to this task can be found in the files \menu{run\_ModelRunner.py} and \menu{plot\_ModelRunner.py} in the \menu{solutions} directory.

\onlyinsubfile{%
    \glssetwidest{1234567}%
    \printnoidxglossary[type=acronym, title=List of Abbreviations, style=mcolalttreenoline]%
    \printbibliography%
    }

\end{document}