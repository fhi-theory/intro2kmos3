\input{preamble.tex}

\title[intro2kmos3]{\Large{Introduction to Kinetic Modeling\\of Stripes, Surfaces, and Solids (\texttt{kmos3})}}

\author[CECAM Workshop]{Martin Deimel, Francesco Cannizzaro, and Roya Ebrahimi Viand}

\institute[]{Fritz Haber Institute of the Max Planck Society}

\date[June 25\textsuperscript{th}, 2024]{June 25\textsuperscript{th}, 2024}

\begin{document}

\frame{
    \titlepage
    }

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\section{Simulation Techniques for Chemical Kinetics}
\begin{frame}
    \frametitle{\insertsectionhead}
        \vspace{0.2cm}
        \centering
        {\small
        \begin{tabular}{l | l | l | l}
            \rowcolor{mpggreen!20}
            &  & (Lattice) Kinetic & Mean-Field \\
            \rowcolor{mpggreen!20}
            & \multirow{-2}{*}{Molecular Dynamics} &  Monte Carlo      & Approximation \\
            \hline\hline
            Treatment &  & Discrete  & \\
            of space  & \multirow{-2}{*}{Continuous 3D} & (lattice) & \multirow{-2}{*}{None} \\
            \rowcolor{mpggreylight} State     & Atomic positions & Occupancies of & Species \\
            \rowcolor{mpggreylight} variables & and velocities   & lattice sites  & coverages \\
            Treatment of & Simulate all vibrations & Markovian state-to- & Rate \\
            reactions    & and barrier crossings   & state dynamics      & equations
        \end{tabular}}
        \vspace{0.5cm}
        \begin{columns}[T]
            \begin{column}{.4\textwidth}
                \centering
                \includegraphics[width=0.75\linewidth]{images/pes.jpg}
            \end{column}%
            \begin{column}{.6\textwidth}
                \textbf{Why Kinetic Monte Carlo?}
                \small
                \begin{itemize}
                    \itemsep-0.2em
                    \item Molecular dynamics limited to short timescales and few "interesting" events.
                    \item Mean-field approximation neglects spatial inhomogeneity.
                \end{itemize}
                \vspace{0.2cm}
                \scriptsize
                \href{https://doi.org/10.1088/0953-8984/27/1/013001}{Stamatakis, \textit{J. Phys.: Condens. Matter} \textbf{27}, 013001 (2015)} (\href{https://creativecommons.org/licenses/by/3.0/}{CC BY 3.0})
            \end{column}%
        \end{columns}
\end{frame}

\section{Applications of Kinetic Monte Carlo Simulations}
\begin{frame}
    \frametitle{\insertsectionhead}
    \vspace{0.2cm}
    \begin{columns}[T]
        \begin{column}{.5\textwidth}
            \textbf{Crystal growth}
            \includegraphics[width=0.9\linewidth,trim={12cm 10cm 0cm 0cm},clip]{images/growth.jpg}
            {\scriptsize
            \href{https://doi.org/10.3389/fchem.2019.00202}{Andersen \textit{et al.}, \textit{Front. Chem.} \textbf{7}, 202 (2019)} (\href{https://creativecommons.org/licenses/by/4.0/}{CC BY 4.0})}\\
            \vspace{0.2cm}
            \textbf{Heterogeneous catalysis}
            \includegraphics[width=0.8\linewidth,trim={5cm 0cm 5cm 0cm},clip]{images/catalysis.png}\\
            \scriptsize
            \href{https://mediatum.ub.tum.de/1692192}{Deimel, "\textit{Ab Initio Studies of the Activity and Selectivity of Transition Metal Catalysts for CO Hydrogenation}," PhD thesis (TU Munich 2023)}
            % MD is allowed to distribute the image from the thesis and allows the MPG to do so
        \end{column}%
        \begin{column}{.5\textwidth}
            \vspace{1cm}
            \textbf{Diffusion in battery materials}
                \includegraphics[width=0.9\linewidth,trim={4cm 25cm 25cm 1.5cm},clip]{images/diffusion.png}
            \scriptsize
            \href{https://doi.org/10.1038/npjcompumats.2016.2}{Urban \textit{et al.}, \textit{npj Comput. Mater.} \textbf{2}, 16002 (2016)} (\href{https://creativecommons.org/licenses/by/4.0/}{CC BY 4.0})
        \end{column}%
        \end{columns}
\end{frame}

\section{Practical Approach}
\begin{frame}
    \frametitle{\insertsectionhead}
    \begin{columns}[T]
        \begin{column}{.55\textwidth}
            \small
            Determine all possible processes in a given system configuration (lattice occupation):
            \begin{itemize}
                \itemsep-0.2em
                \item Adsorption
                \item Desorption
                \item Diffusion
                \item Reaction
            \end{itemize}
            Kinetic parameters needed as input:
            \begin{itemize}
                \itemsep-0.2em 
                \item Experimental data
                \item First-principles calculations
            \end{itemize}
            Achieve numerical solution to the master equation:
                \[
                    \frac{d P_i(t)}{dt} = - \sum_j k_{i \rightarrow j} P_i(t)  + \sum_j k_{j \rightarrow i} P_j(t)
                \]
        \end{column}%
        \begin{column}{.45\textwidth}
            \vspace{1cm}
            \href{https://www.fhi.mpg.de/505207/kinetic_monte_carlo}{\includegraphics[width=0.8\linewidth]{images/processes.png}}
        \end{column}%
    \end{columns}
\end{frame}

\section{Variable Step-Size Method (VSSM)}
\begin{frame}
    \frametitle{\insertsectionhead}
    \vspace{0.2cm}
    \centering
    \includegraphics[width=0.7\linewidth]{images/vssm.png}
\end{frame}

\section{The \texttt{kmos3} framework}
\begin{frame}
    \frametitle{\insertsectionhead}
    \begin{itemize}
        \small
        \itemsep-0.2em
        \item Highly efficient Fortran90 code generated from abstract model definition in Python.
        \item Allows for efficient local updates of enabled and disabled events (required for VSSM).
        \item Storage and exchange of kMC models through XML data format.
        \item Python interface (f2py) to run and evaluate the model.
        \item Graphical user interface (GUI) and application programming interface (API).
    \end{itemize}
    \vspace{0.2cm}
    \centering
    \includegraphics[height=4cm]{images/kmos3_structure.png}
\end{frame}

\section{\texorpdfstring{Example: CO Oxidation @ RuO\textsubscript{2}(110)}{Example: CO Oxidation @ RuO2(110)}}
\begin{frame}
    \frametitle{\insertsectionhead}
    \begin{columns}[T]
        \begin{column}{.65\textwidth}
            \centering
            \includegraphics[width=0.75\linewidth,trim={7.2cm 0 0 0},clip]{images/co_oxi.png}
        \end{column}%
        \begin{column}{.35\textwidth}
            \vspace{1.9cm}
            Surface structure
        \end{column}%
    \end{columns}
    \begin{columns}[T]
        \begin{column}{.3\textwidth}
            \vspace{1.6cm}\hspace{0.5cm}
            \centering
            DFT input data
        \end{column}%
        \begin{column}{.7\textwidth}
            \vspace{0.2cm}
            {\small
            \begin{tabular}{l | c | c | c | c | c | c}
                \rowcolor{mpggreen!20}
                & & \multicolumn{3}{c|}{$\Delta E^{\text{des}}$} & \multicolumn{2}{c}{$\Delta E^{\text{diff}}$} \\
                \rowcolor{mpggreen!20}
                & \multirow{-2}{*}{$E_b$} & \multicolumn{1}{c}{unimol.} & \multicolumn{1}{c}{with $\text{O}^{\text{br}}$} & with $\text{O}^{\text{cus}}$ & \multicolumn{1}{c}{to br} & to cus \\
                \hline\hline
                $\text{CO}^{\text{br}}$  & $-$1.6 &      1.6 & 1.5 & 0.8 & 0.6 & 1.6 \\
                \rowcolor{mpggreylight} $\text{CO}^{\text{cus}}$ & $-$1.3 &      1.3 & 1.2 & 0.9 & 1.3 & 1.7 \\
                $\text{O}^{\text{br}}$   & $-$2.3 & $\cdots$ & 4.6 & 3.3 & 0.7 & 2.3 \\
                \rowcolor{mpggreylight} $\text{CO}^{\text{cus}}$ & $-$1.0 & $\cdots$ & 3.3 & 2.0 & 1.0 & 1.6
            \end{tabular}}
        \end{column}%
    \end{columns}
    \vspace{0.1cm}\hspace{0.5cm}
    \scriptsize
    \href{https://doi.org/10.1103/PhysRevB.73.045433}{Reuter and Scheffler, \textit{Phys. Rev. B} \textbf{73}, 045433 (2006)} (\href{https://creativecommons.org/licenses/by/3.0/}{CC BY 3.0})
\end{frame}

\section{TASK: The Viewer GUI}
\begin{frame}
    \frametitle{\insertsectionhead}
    \begin{columns}[T]
        \begin{column}{.5\textwidth}
            \begin{block}{Objective}
                Get used to quickly observe model features using the kmos3 viewer GUI
            \end{block}
            \begin{block}{}
                \begin{itemize}
                    \item Start the viewer by follow the instructions 1 and 2 in the handout
                    \item Familiarize yourself with the information in the two windows
                    \item Use the sliders to alter the model parameters and observe the effect on reactivity and surface state
                    \item Follow the remaining instructions in the handout
                \end{itemize}
           \end{block}
        \end{column}%
        \begin{column}{.45\textwidth}
            \vspace{0.3cm}
            \begin{figure}
                \raggedleft
                \includegraphics[width=.8\linewidth]{images/kmos3_viewer.png}
                \makebox[0pt][r]{
                    \raisebox{-.4\linewidth}{%
                      \includegraphics[width=.65\linewidth]{images/ase_view.png}
                    }\hspace*{.25\linewidth}%
                }
                \end{figure}
        \end{column}%
    \end{columns}
\end{frame}

\section{TASK: Arrhenius Plots}
\begin{frame}
    \frametitle{\insertsectionhead}
        \begin{block}{Client scripts}
            \begin{itemize}
                \item Most efficient way to use kmos3
                \item Automatize tasks
                \item Fine access to all model features
                \item Easy integration with other python tools (e.g., plotting with Matplotlib)
            \end{itemize}
        \end{block}
        \begin{block}{Example}
            Study dependence of reactivity (turnover frequency, TOF) on the temperature for fixed partial pressures. Plot log$_{10}$(TOF) vs. 1/T (Arrhenius plot).
       \end{block}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{block}{Initialization}
            \begin{itemize}
                \item Import and initialize an instance of \texttt{kmos3.run.KMC\_Model}\\
                \begin{lstlisting}
from kmos3.run import KMC_Model
model = KMC_Model(banner=False)
\end{lstlisting}
            \end{itemize}
        \end{block}
        \begin{block}{General setup}
            \begin{itemize}
                \item Set the pressure parameters to the desired values\\
                \begin{lstlisting}
model.parameters.p_COgas = 2.e-1
model.parameters.p_O2gas = 1.e-1
\end{lstlisting}
            \end{itemize}
       \end{block}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{block}{Main calculation loop}
            \begin{itemize}
                \item Define the number of relaxation and sample steps (numerical parameters)
                \item Generate a list of temperature values, and an empty list to store the calculated TOFs
                \item Run the model in a loop for each temperature\\
                \begin{lstlisting}
nrel = 1e7
nsample = 1e7
Ts = range(450, 650, 20)
TOFs = []
for T in Ts:
    model.parameters.T = T
    model.do_steps(nrel)
    output = model.get_std_sampled_data(1, nsample, output='dict')
    TOFs.append(output['CO_oxidation'])
\end{lstlisting}
            \end{itemize}
       \end{block}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{block}{Plot}
            \begin{itemize}
                \item Transform the variables
                \item Plot the results using Matplotlib\\
                \begin{lstlisting}
invTs = [1/float(T) for T in Ts]
logTOFs = [math.log(TOF,10.) for TOF in TOFs]
import matplotlib.pyplot as plt
fig = plt.figure()
axis = fig.add_subplot(111)
axis.plot(invTs, logTOFs, '-o')
axis.set_title('Arrhenius plot')
axis.set_xlabel('1/T [1/K]')
plt.ylabel('log(TOF) [events (sites s)^-1]')
plt.show()
\end{lstlisting}
            \end{itemize}
       \end{block}
\end{frame}
\begin{frame}
    \frametitle{\insertsectionhead}
        \centering
        \includegraphics[width=0.48\linewidth]{images/arrhenius.pdf}\hspace{0.04\linewidth}%
        \phantom{\includegraphics[width=0.48\linewidth]{images/vs_pCO.pdf}}
\end{frame}
\begin{frame}
    \frametitle{TOF and Coverages vs. Pressure}
        \centering
        \includegraphics[width=0.48\linewidth]{images/arrhenius.pdf}\hspace{0.04\linewidth}%
        \includegraphics[width=0.48\linewidth]{images/vs_pCO.pdf}
            
\end{frame}

\section{TASK: \texorpdfstring{O\textsubscript{2} Adsorption / Desorption}{O2 Adsorption / Desorption}}
\begin{frame}
    \frametitle{\insertsectionhead}
        \centering
        \includegraphics[height=0.8\textheight]{images/kmos3_model.png}
\end{frame}
\begin{frame}
    \frametitle{\insertsectionhead}
        \begin{block}{Example}
            Dissociative adsorption and associative desorption of O$_2$ on a square lattice
        \end{block}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{block}{Objectives}
                    \begin{itemize}
                        \item Understand all basic elements of a \texttt{kmos3} project: lattice, sites, species, parameters and processes
                        \item Generate and understand the model file
                        \item Learn to export the model
                    \end{itemize}
               \end{block}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
        
\end{frame}
\begin{frame}
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{block}{Example}
            Dissociative adsorption and associative desorption of O$_2$ on a square lattice
        \end{block}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{block}{Objectives}
                    \begin{itemize}
                        \item Understand all basic elements of a \texttt{kmos3} project: lattice, sites, species, parameters and processes
                        \item Generate and understand the model file
                        \item Learn to export the model
                    \end{itemize}
               \end{block}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Imports}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
import kmos3
from kmos3.types import *
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Initialize a new model}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
import kmos3
from kmos3.types import *
kmc_model = kmos3.create_kmc_model(
    'O2adsdes')
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Set projects metadata}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
import kmos3
from kmos3.types import *
kmc_model = kmos3.create_kmc_model(
    'O2adsdes')
kmc_model.set_meta(
    author='',
    email='',
    model_name='O2_adsdes',
    model_dimension=2)
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Define a layer}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
layer = Layer(name='fcc100')
site = Site(name='hol',
    pos='0.5 0.5 0.5')
layer.sites.append(site)
kmc_model.add_layer(layer)
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Add surface species}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
layer = Layer(name='fcc100')
site = Site(name='hol',
    pos='0.5 0.5 0.5')
layer.sites.append(site)
kmc_model.add_layer(layer)
kmc_model.add_species(name='empty',
    color='#dddddd')
kmc_model.add_species(name='O',
    color='#ff0000',
    representation= "Atoms('O', [[0.,0.,0.]])")
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Define model parameters}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
kmc_model.add_parameter(name='kads',
    value=1.0)
kmc_model.add_parameter(name='kdes',
    value=1.0, adjustable=True,
    min=1e-2, max=1e2, scale='log')
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Generate auxiliary coordinates}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
kmc_model.add_parameter(name='kads',
    value=1.0)
kmc_model.add_parameter(name='kdes',
    value=1.0, adjustable=True,
    min=1e-2, max=1e2, scale='log')
center = kmc_model.lattice.generate_coord(
    'hol')
right = kmc_model.lattice.generate_coord(
    'hol.(1,0,0)')
up = kmc_model.lattice.generate_coord(
    'hol.(0,1,0)')
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Define the processes}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
kmc_model.add_process(name=...,
    conditions=[...],
    actions=[...],
    rate_constant=...,
    tof_count={...})
...
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\insertsectionhead}
        \begin{tikzpicture}[remember picture,overlay]
            \node[xshift=0cm,yshift=0.1\textheight,anchor=south east,at=(current page.south east)] {
                \includegraphics[height=0.28\linewidth]{images/o2_model.png}
            };
        \end{tikzpicture}
        \begin{columns}[T]
            \begin{column}{.6\textwidth}
                \begin{minipage}[t][\textheight][t]{\textwidth}
                    \begin{block}{Save and compile the model}
                        \begin{minipage}[t][0.6\textheight][t]{\textwidth}
                        \begin{lstlisting}
kmc_model.add_process(name=...,
    conditions=[...],
    actions=[...],
    rate_constant=...,
    tof_count={...})
...

kmc_model.save_model()
kmos3.compile(kmc_model)

# DONE
\end{lstlisting}
                        \end{minipage}
                   \end{block}
               \end{minipage}
            \end{column}
            \begin{column}{.4\textwidth}
                \hfill
            \end{column}
        \end{columns}
\end{frame}


\end{document}
