"""
Demonstration of a kmos render scrikmc_model.
Generates a simple model for simulating
dissociative adsorption / associative desorption
of O2 on a FCC(100) (square) lattice

Juan M. Lorenzi
TU Munich
June 2016

Martin Deimel
FHI
June2024
"""

# Imports
import kmos3
from kmos3.types import *

# Initialize a new model
kmc_model = kmos3.create_kmc_model('O2adsdes')

# Set projects metadata
kmc_model.set_meta(author='Martin Deimel',
    email='',
    model_name='O2adsdes',
    model_dimension=2)

# Define a layer
layer = Layer(name='fcc100')
site = Site(name='hol', pos='0.5 0.5 0.5')
layer.sites.append(site)
kmc_model.add_layer(layer)

# Add surface species
kmc_model.add_species(name='empty', color='#dddddd')
kmc_model.add_species(name='O', color='#ff0000',
    representation="Atoms('O',[[0.,0.,0.]])")

# Define model parameters
kmc_model.add_parameter(name='kads', value = 1.0)
kmc_model.add_parameter(name='kdes', value=1.0,
    adjustable=True, min=1e-2, max=1e2, scale='log')

# Generate auxiliary coordinates
center = kmc_model.lattice.generate_coord('hol')
right = kmc_model.lattice.generate_coord('hol.(1,0,0)')
up = kmc_model.lattice.generate_coord('hol.(0,1,0)')

# Define the processes
kmc_model.add_process(name='O2_ads_right',
    conditions=[Condition(coord=center, species='empty'),
                Condition(coord=right, species='empty'),],
    actions=[Action(coord=center, species='O'),
             Action(coord=right, species='O'),],
    rate_constant='0.5*kads',
    tof_count={'O2_ads': 1})

kmc_model.add_process(name='O2_ads_up',
    conditions=[Condition(coord=center, species='empty'),
                Condition(coord=up, species='empty'),],
    actions=[Action(coord=center, species='O'),
             Action(coord=up, species='O'),],
    rate_constant='0.5*kads',
    tof_count={'O2_ads': 1})

kmc_model.add_process(name = 'O2_des_right',
    conditions=[Condition(coord=center, species='O'),
                Condition(coord=right, species='O'),],
    actions=[Action(coord=center, species='empty'),
             Action(coord=right, species='empty'),],
    rate_constant='0.5*kdes',
    tof_count={'O2_ads': -1})

kmc_model.add_process(name='O2_des_up',
    conditions=[Condition(coord=center, species='O'),
                Condition(coord=up, species='O'),],
    actions=[Action(coord=center, species='empty'),
             Action(coord=up, species='empty'),],
    rate_constant='0.5*kdes',
    tof_count={'O2_ads': -1})

# Alternatively, define processes programatically
# for i, coord in enumerate([right, up]):
#     ads_conds = [Condition(coord=center, species='empty'),
#                  Condition(coord=coord, species='empty')]
#     ads_acts  = [Action(coord=center, species='O'),
#                  Action(coord=coord, species='O')]
#     # Adsorption
#     kmc_model.add_process(name = 'O2_ads_{:02d}'.format(i),
#                    conditions = ads_conds,
#                    actions = ads_acts,
#                    rate_constant = '0.5*kads',
#                    tof_count = {'O2_ads': 1})
#     # Desorption
#     kmc_model.add_process(name = 'O2_des_{:02d}'.format(i),
#                    conditions = ads_acts,
#                    actions = ads_conds,
#                    rate_constant = '0.5*kdes',
#                    tof_count={'O2_ads': -1})

# Save and compile the model
kmc_model.save_model()
kmos3.compile(kmc_model)
