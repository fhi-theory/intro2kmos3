"""
Generate an Arrhenius plot
using kmos3

Juan M. Lorenzi
TU Munich
June 2016

Martin Deimel
FHI
June 2024
"""
import math

from kmos3.run import KMC_Model
model = KMC_Model(banner = False)

model.parameters.p_COgas = 2.e-1
model.parameters.p_O2gas = 1.e-1

nrel = 1e7                 # relaxation steps
nsample = 1e7              # sample steps
Ts = range(450,650,20)     # increments of 20 between 450 and 650 K
TOFs = []                  # empty list for output
# Loop over the temperature
for T in Ts:
    model.parameters.T = T   # Set the temperature
    model.do_steps(nrel)     # Relax the system
    # Sample the reactivity
    output = model.get_std_sampled_data(1, nsample, output='dict')
    # Collect output
    TOFs.append(output['CO_oxidation'])

# Transform the variables
invTs = [1/float(T) for T in Ts]
logTOFs = [math.log(TOF,10.) for TOF in TOFs]

# Plot the data
import matplotlib.pyplot as plt
fig = plt.figure()
axis = fig.add_subplot(111)
axis.plot(invTs, logTOFs, '-o')
def inverse(x):
    return 1 / x
axis.set_title('Arrhenius plot')
axis.set_xlabel('1/T [1/K]')
sec_axis = axis.secondary_xaxis('top', functions=(inverse, inverse))
sec_axis.set_xlabel('T [K]')
plt.ylabel('$\\mathrm{log}_{10} \\left( \\mathrm{TOF} \\left[ \\frac{ \\mathrm{events} }{ \\mathrm{sites} \\times \\mathrm{s}} \\right] \\right)$')
plt.tight_layout()
plt.savefig('arrhenius.pdf') # Optionally, save plot
plt.show()
